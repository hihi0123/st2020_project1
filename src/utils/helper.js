let sorting = (array) => {
    return array.sort();        
}

let compare = (a, b) => {
        
    return a["PM2.5"]-b["PM2.5"];
}

let average = (nums) => {
	var len = nums.length;
	var sum = 0;
	for(var i = 0; i < len ; i++){
	 if (nums[i]=== null || nums[i]=== undefined || isNaN(nums[i])){
	 	len = len -1;}
	else{

	sum = sum+nums[i];
	}
		        }
	return Math.floor(sum/len*100)/100;
}


module.exports = {
    sorting,
    compare,
    average
}
